extends Gun_Base

signal created_projectile(card)

export(PackedScene) var slash_scene

var x : int

func _physics_process(delta):
	rotate_gun()

	if (get_global_mouse_position().x - self.global_position.x) < 0:
		$Sprite.flip_v = true
		x = -1
	else:
		$Sprite.flip_v = false
		x = 1

func shoot():
	if not $Cooldown.is_stopped():
		return
	$Cooldown.start()

	$Tween.interpolate_property($Sprite, "modulate", Color8(189,189,189), Color8(138,6,6), 0.3, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	$Tween.interpolate_property(self, "rotation_degrees", self.rotation_degrees, self.rotation_degrees - 60*x, 0.3, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	$Tween.start()

func _on_Tween_tween_all_completed():
	var slash = slash_scene.instance()
	slash.setup($Sprite.global_position, rotation_degrees)
	emit_signal("created_projectile", slash)
	$Sprite.modulate = Color8(189,189,189)
