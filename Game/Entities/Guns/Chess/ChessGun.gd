extends Gun_Base

signal created_projectile(dice)

export(PackedScene) var piece_scene

onready var frames : int = 1002
onready var left : bool
var x : int

func _physics_process(delta):
	rotate_gun()
	while $Timer.is_stopped():
		frames += 1
		$Timer.start()
		if frames == 1008:
			frames = 1002

	if (get_global_mouse_position().x - self.global_position.x) < 0:
		$Sprite.flip_v = true
		$CPUParticles2D.position.y = 6
		x = -1
		left = true
	else:
		$Sprite.flip_v = false
		$CPUParticles2D.position.y = -6
		x = 1
		left = false

func shoot():
	if !$Cooldown.is_stopped():
		return

	$CPUParticles2D.emitting = true
	$Tween.interpolate_property(self, "rotation_degrees", self.rotation_degrees, self.rotation_degrees - x*45, 0.6,Tween.TRANS_BACK, Tween.EASE_IN_OUT)
	$Tween.start()

	$Cooldown.start()

func _on_Tween_tween_all_completed():
	var chess_piece = piece_scene.instance()
	chess_piece.setup($Sprite.global_position, rotation_degrees, frames, left)
	emit_signal("created_projectile", chess_piece)
	$CPUParticles2D.emitting = false
