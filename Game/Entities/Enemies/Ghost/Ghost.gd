extends Enemy

var movement : Vector2
var speed : int = 300
var invisible : bool = false

var knockback_multiplier : int = 1
var on_knockback : bool = false
var knockback_timer : float = 0.0
const KNOCKBACK_DUR : float = 0.2

func _ready():
	$AnimationPlayer.get_animation("dance").set_loop(true)
	$AnimationPlayer.play("dance")

func _physics_process(delta):
	var vector = (hero.global_position - self.global_position).normalized()

	if not invisible:
		self.rotation = vector.angle()
		if vector.x < 0:
			self.rotation -= PI
		movement = vector * speed
	else:
		movement = Vector2.ZERO

	if on_knockback:
		movement *= -1 * knockback_multiplier
		knockback_timer += delta

		if knockback_timer >= KNOCKBACK_DUR:
			knockback_timer = 0
			on_knockback = false

	self.move_and_slide(movement)

func damage(value, knockback = false):
	.damage(value)

	if knockback:
		on_knockback = true
		knockback_multiplier = 2
		$Timer.start()

func _on_Hitbox_body_entered(body):
	body.damage(self.global_position)
	on_knockback = true

func _on_Timer_timeout():
	knockback_multiplier = 1

func _on_Visible_time_timeout():
	invisible = !invisible
	$CollisionShape2D.disabled = !$CollisionShape2D.disabled
	$Hitbox/CollisionShape2D.disabled = !$Hitbox/CollisionShape2D.disabled
	if invisible:
		$AnimationPlayer.play("invisibility")
	else:
		$AnimationPlayer.play("dance")
