extends Node2D

onready var enemies = $Enemies
onready var dialogue = $CanvasLayer/Dialogue_Popup

var hero : Hero
var enemy_counter : int
var enemies_killed : int = 0
var played : bool = false

signal proceed()
signal back()
signal monsterfy()

func _ready():
	enemy_counter = enemies.get_child_count()
	
	for enemy in enemies.get_children():
		enemy.hero = hero
		enemy.connect("died", self, "_on_enemy_died")

func _on_enemy_died():
	enemies_killed += 1

func _process(_delta):
	if enemies_killed > 1 and !played:
		dialogue.open()
		played = true
	
	if enemies_killed >= 1 and hero.position.y < 10:
		emit_signal("proceed")
	
	if enemies_killed == 0 and hero.position.y < 10:
		pass
	
	if hero.gun == null and hero.position.y < 20:
		emit_signal("back")
