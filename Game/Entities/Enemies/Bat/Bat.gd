extends Enemy

var speed : int = 400
var vector : Vector2
var movement : Vector2
var direction : int = -1

var rng = RandomNumberGenerator.new()

func _ready():
	rng.randomize()
	$AnimationPlayer.play("fly")

func _physics_process(delta):
	vector = (hero.global_position - self.global_position).normalized()
	movement = vector * speed * direction

	self.move_and_slide(movement)

func _on_VisibilityNotifier2D_screen_exited():
	if $Timer.is_inside_tree():
		$Timer.start()
	else:
		$Timer.autostart = true

	yield($Timer, "timeout")
	direction = 1
	var number = rng.randi_range(0, 3)
	match number:
		0:
			self.global_position = Vector2(-50, rng.randf_range(0, 720))
		1:
			self.global_position = Vector2(1330, rng.randf_range(0, 720))
		2:
			self.global_position = Vector2(rng.randf_range(0, 1280), -50)
		3:
			self.global_position = Vector2(rng.randf_range(0, 1280), 770)

func _on_Hitbox_body_entered(body):
	body.damage(self.global_position)
	direction = -1

func damage(value, knockback = false):
	.damage(value)
	direction = -1
