extends Gun_Base

signal created_projectile(proj)

onready var anim_player = $AnimationPlayer
onready var timer = $Timer
onready var note_position = $NotePosition

export(PackedScene) var projectile_scene

var can_shoot = true


func _process(_delta):
	if Input.is_action_pressed("hero_shoot"):
		if anim_player.current_animation != "music":
			anim_player.play("music")
		if can_shoot:
			shoot()
	else:
		anim_player.play("default")


func shoot():
	var projectile = projectile_scene.instance()
	projectile.position = note_position.global_position
	emit_signal("created_projectile", projectile)
	can_shoot = false
	timer.start()


func _on_Timer_timeout():
	can_shoot = true
