extends Control

onready var back_btn = $VBoxContainer/Back

signal go_back()

func _ready():
	back_btn.connect("pressed", self, "_on_back")
	
	"""
	$Tween.interpolate_property(	$Panel, "rect_position",
								Vector2.ZERO, OS.window_size/2,
								1.5, Tween.TRANS_ELASTIC)
	$Tween.interpolate_property(	$Panel, "rect_size",
								OS.window_size, Vector2.ZERO,
								1.5, Tween.TRANS_ELASTIC)
	$Tween.start()
	"""

func _on_Label_mouse_entered():
	$VBoxContainer/Back/Label.set("custom_colors/font_color", Color(0, 1, 0))

func _on_Label_mouse_exited():
	$VBoxContainer/Back/Label.set("custom_colors/font_color", Color(1, 1, 1))

func _on_back():
	emit_signal("go_back")

func _on_go_credits():
	$Tween.interpolate_property(	$Panel, "rect_position",
								Vector2.ZERO, OS.window_size/2,
								1.5, Tween.TRANS_ELASTIC)
	$Tween.interpolate_property(	$Panel, "rect_size",
								OS.window_size, Vector2.ZERO,
								1.5, Tween.TRANS_ELASTIC)
	$Tween.start()
