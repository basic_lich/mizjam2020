extends Control

onready var start_btn = $Menu/CenterRow/Buttons/Start_Game
onready var credits_btn = $Menu/CenterRow/Buttons/Credits
onready var exit_btn = $Menu/CenterRow/Buttons/Exit

var start_scene : PackedScene
var emission : String = ""

signal go_credits()

func _ready():
	start_btn.connect("pressed", self, "_on_start_game", [start_btn.scene_to_load])
	credits_btn.connect("pressed", self, "_on_credits")
	exit_btn.connect("pressed", self, "_on_exit")

func _on_start_game(scene_to_load):
	start_scene = scene_to_load
	Tutorial_music.play()
	changing_scene()

func _on_credits():
	emit_signal("go_credits")

func _on_exit():
	get_tree().quit()

func changing_scene():
	$Menu.hide()

	$Tween.interpolate_property(	$Panel, "rect_position",
								OS.window_size/2, Vector2.ZERO,
								0.5, Tween.TRANS_EXPO)
	$Tween.interpolate_property(	$Panel, "rect_size",
								Vector2.ZERO, OS.window_size,
								0.5, Tween.TRANS_EXPO)
	$Tween.start()

func _on_Tween_tween_all_completed():
	get_tree().change_scene_to(start_scene)
	$Panel.rect_size = Vector2.ZERO
	emit_signal(emission)

