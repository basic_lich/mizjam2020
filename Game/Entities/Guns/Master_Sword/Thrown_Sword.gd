extends Sprite

var damage : int = 4

const SPEED : int = 500
const ROT_SPEED : int = 1500

var spin_dir : int = 1
var direction : Vector2 = Vector2(1, 0)

var collided : bool = false

func setup(pos : Vector2, set_rotation: float, set_frame : int, color : Color, left : bool):
	self.global_position = pos
	self.rotation = set_rotation

	self.frame = set_frame
	self.modulate = color

	if left: spin_dir = -1

	direction = direction.rotated(set_rotation)

func _physics_process(delta):
	if not collided:
		position += SPEED * delta * direction
		self.rotation_degrees += ROT_SPEED * spin_dir * delta
		if abs(self.rotation_degrees) > 360:
			self.rotation_degrees = abs(self.rotation_degrees) - 360

func _on_Hitbox_body_entered(body):
	if body is Enemy:
		body.damage(3)
	$Hitbox/Area.call_deferred("set", "disabled", true)
	collided = true
	$Tween.interpolate_property(self, "modulate:a", 1, 0, 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()

func _on_Tween_tween_all_completed():
	self.call_deferred("free")

func _on_Na_Tela_exited():
	self.call_deferred("free")
