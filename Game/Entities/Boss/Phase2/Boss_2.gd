extends Enemy

var accel : float = 400.0
var speed : Vector2 = Vector2.ZERO

export(PackedScene) var projectiles : PackedScene
signal created_projectile(projectile)

func _physics_process(delta):
	var dist = (hero.global_position - self.global_position).normalized()

	speed += dist*accel*delta

	self.move_and_slide(speed)
	self.rotation = speed.angle() - PI/2

func _on_Head_Glitch_timeout():
	$Head.frame = randi()%8+504
	$Head.position = Vector2(rand_range(-10,10), rand_range(-410, -430))
	$Head.scale = Vector2(rand_range(5.5,15), rand_range(5.5, 15))
	$Head.rotation = rand_range(-PI/4, PI/4)

func _on_Hitbox_body_entered(body):
	body.damage(self.global_position)

func _on_Shoot_timeout():
	var projectile_1 = projectiles.instance()
	projectile_1.global_position = $Left_Eye.global_position
	projectile_1.hero = self.hero
	emit_signal("created_projectile", projectile_1)

	var projectile_2 = projectiles.instance()
	projectile_2.global_position = $Right_Eye.global_position
	projectile_2.hero = self.hero
	projectile_2.speed = (hero.global_position - $Right_Eye.global_position).normalized()
	emit_signal("created_projectile", projectile_2)

func damage(dmg):
	.damage(dmg)
	if health < max_health/2:
		self.modulate = Color(1,0,0)
		accel = 800.0
