extends Gun_Base

signal created_projectile(bolt)

onready var sprite = $Sprite
onready var cooldown = $Cooldown

export(PackedScene) var bolt_scene

func _physics_process(delta):
	rotate_gun()

func shoot():
	if not cooldown.is_stopped():
		return

	var bolt = bolt_scene.instance()
	bolt.setup(sprite.global_position, rotation_degrees)
	emit_signal("created_projectile", bolt)
	cooldown.start()
