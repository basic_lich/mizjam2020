extends Node2D

export(float) var speed = 1000
export(int) var damage_value = 3

var direction := Vector2(1, 0)


func _process(delta):
	position += speed * delta * direction


func setup(pos:Vector2, angle:float) -> void:
	position = pos
	rotation_degrees = angle
	direction = direction.rotated(deg2rad(angle))


func _on_Area2D_body_entered(body):
	$AnimationPlayer.play("die")
	set_process(false)

	if body is Enemy:
		(body as Enemy).damage(damage_value)


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
