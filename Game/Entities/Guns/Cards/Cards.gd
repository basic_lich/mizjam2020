extends Gun_Base

signal created_projectile(card)

onready var timer = get_node("Timer")
onready var sprite = get_node("Sprite")
onready var tween = get_node("Tween")
onready var cooldown = get_node("Cooldown")

var shooting : bool = false
var color = Color(1,0,0)
var frames = [788, 884, 836, 932]
onready var first = 788

export(PackedScene) var card_scene

func _physics_process(delta):
	rotate_gun()
	while timer.is_stopped():
		sprite.frame += 1
		timer.start()
		if sprite.frame == first + 12:
			sprite.frame = first
			timer.start()

func shoot():
	if shooting:
		return
	cooldown.start()
	shooting = true

	var x = 0
	if color == Color(1,0,0):
		color = Color(1,1,1)

	else:
		color = Color(1,0,0)

	for i in range(1, 5):
		var card = card_scene.instance()
		card.setup(sprite.global_position, rotation_degrees - 45 + x, sprite.frame, sprite.modulate)
		emit_signal("created_projectile", card)
		x += 30

	sprite.modulate = color
	sprite.modulate.a = 0
	for i in range(0,4):
		if first == frames[i]:
			if i == 3:
				first = frames[0]
			else:
				first = frames[i+1]
			break
	sprite.frame = first

func _on_Cooldown_timeout():
	tween.interpolate_property(sprite, "modulate:a", 0, 1, 0.2)
	tween.interpolate_property(sprite, "rotation_degrees", -270, 90, 0.5)
	tween.interpolate_property(sprite, "scale", Vector2(0.01, 0.01), Vector2.ONE, 0.5, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
	tween.start()

func _on_Tween_tween_all_completed():
	shooting = false
