extends Area2D

signal more_life
var on_area : bool

func _ready():
	$AnimationPlayer.play("beat")
	get_parent().get_node("Restore").connect("restore", self, "_pickup")

func _on_MoreLife_body_entered(body):
	$Pickup_Button.show()
	on_area = true

func _on_MoreLife_body_exited(body):
	$Pickup_Button.hide()
	on_area = false

func _input(event):
	if on_area and event.is_action_pressed("hero_pickup"):
		emit_signal("more_life")
		self.queue_free()

func _pickup():
	self.queue_free()
