extends Node2D

var frames = [910, 948, 951, 903, 911, 907, 949, 952, 954, 911]

var direction := Vector2(1, 0)
var speed : int = 300
var damage : float = 2
var animation : bool = true

func _ready():
	if animation:
		$AnimationPlayer.get_animation("up_and_down").set_loop(true)
		$AnimationPlayer.play("up_and_down")

func _process(delta):
	position += speed * delta * direction


func setup(pos:Vector2, angle:float, left, cont) -> void:
	$Sprite.frame = frames[cont]
	position = pos
	rotation_degrees = angle
	direction = direction.rotated(deg2rad(angle))
	if left:
		$Sprite.rotation_degrees += 180

func _on_Hitbox_body_entered(body):
	if(body.has_method("damage")):
		body.damage(damage)
	animation = false
	$Tween.interpolate_property(self, "modulate:a", 1, 0, 1, Tween.TRANS_QUART, Tween.EASE_IN_OUT)
	$Tween.start()
	$Sprite/Hitbox.queue_free()
	self.queue_free()

func _on_Tween_tween_all_completed():
	self.queue_free()


func _on_VisibilityNotifier2D_screen_exited():
	self.queue_free()
