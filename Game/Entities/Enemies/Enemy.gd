extends KinematicBody2D
class_name Enemy

signal died()

var hero : Hero

export var max_health := 5
onready var health := max_health

func die():
	self.emit_signal("died")
	self.call_deferred("free")

func damage(value):
	health -= value
	if health <= 0:
		die()

func is_enemy() -> bool:
	return true
