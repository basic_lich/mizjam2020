extends Button

onready var label = $Label

func _on_Label_mouse_entered():
	label.set("custom_colors/font_color", Color(0, 1, 0))

func _on_Label_mouse_exited():
	label.set("custom_colors/font_color", Color(1, 1, 1))

