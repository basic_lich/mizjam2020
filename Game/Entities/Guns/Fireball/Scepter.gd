extends Gun_Base

signal created_projectile(card)

onready var sprite = get_node("Sprite")
onready var tween = get_node("Tween")
onready var cooldown = get_node("Cooldown")

var colors = [Color(1,0,0), Color(0,1,0), Color(0,0,1)]
var shooting : bool = false
var x : int
var rng = RandomNumberGenerator.new()
var n : int

export(PackedScene) var ball_scene

func _ready():
	rng.randomize()

func _physics_process(delta):
	rotate_gun()

	if (get_global_mouse_position().x - self.global_position.x) < 0:
		$Sprite.flip_v = true
		$CPUParticles2D.position.y = 6
		x = -1
	else:
		$Sprite.flip_v = false
		$CPUParticles2D.position.y = -6
		x = 1

func shoot():
	if not cooldown.is_stopped():
		return
	cooldown.start()
	n = rng.randi_range(0, 2)

	$CPUParticles2D.modulate = colors[n]
	$CPUParticles2D.emitting = true
	$Tween.interpolate_property(self, "rotation_degrees", self.rotation_degrees, self.rotation_degrees - x*45, 0.4,Tween.TRANS_BACK, Tween.EASE_IN_OUT)
	$Tween.start()

func _on_Tween_tween_all_completed():
	var ball = ball_scene.instance()
	ball.setup(sprite.global_position, rotation_degrees, $CPUParticles2D.modulate, n)
	emit_signal("created_projectile", ball)
	$CPUParticles2D.emitting = false
