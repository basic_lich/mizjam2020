extends Enemy

var speed : int = 300
var vetor : Vector2
var movement : Vector2

var walking : bool = false

var knockback_multiplier : int = 1
var on_knockback : bool = false
var knockback_timer : float = 0.0
const KNOCKBACK_DUR : float = 0.2

func _physics_process(delta):
	vetor = hero.global_position - self.global_position
	self.rotation = vetor.angle()
	if walking:
		movement = vetor.normalized() * speed
	else:
		movement = Vector2.ZERO

	if on_knockback:
		movement *= -1 * knockback_multiplier
		knockback_timer += delta

		if knockback_timer >= KNOCKBACK_DUR:
			knockback_timer = 0
			on_knockback = false

	# warning-ignore:return_value_discarded
	self.move_and_slide(movement)

func damage(value, knockback = false):
	.damage(value)

	if knockback:
		on_knockback = true
		knockback_multiplier = 2
		$Timer.start()
	yield($Timer, "timeout")

func _on_Hitbox_body_entered(body):
	body.damage(self.global_position)
	on_knockback = true

func _on_Timer_timeout():
	knockback_multiplier = 1

func _on_EmergeBox_body_entered(body):
	$AnimationPlayer.play("emerge")
	$Emerging.start()
	walking = false

func _on_Emerging_timeout():
	$BodyShow.start()
	$CollisionShape2D.scale = Vector2.ONE

func _on_BodyShow_timeout():
	$AnimationPlayer.play("dig")
	$Digging.start()

func _on_Digging_timeout():
	walking = true
	$AnimationPlayer.play("walk")
