extends Gun_Base

export(PackedScene) var glitch_bullet : PackedScene

var cooldown : bool = false
signal created_projectile(glitch)

func _physics_process(_delta):
	rotate_gun()
	$Sprite.rotation = -self.rotation

	if not cooldown and Input.is_action_pressed("hero_shoot"):
		cooldown = true
		var bullet = glitch_bullet.instance()
		bullet.global_position = $Sprite.global_position
		bullet.dir = Vector2(1,0).rotated(self.rotation)
		emit_signal("created_projectile", bullet)

func _on_Fire_rate_timeout():
	cooldown = false
