extends Node2D

onready var chest = $Furniture/Furniture_Sprites/Chest
onready var pickup = $Gun_Pickup
onready var gun_sprite = $Gun_Pickup/Sprite
onready var talk = $Talk_Button
onready var dialogue = $CanvasLayer/Dialogue_Popup

var hero : Hero
var been_there : bool = false

signal proceed()
signal back()
signal monsterfy()

func _ready():
	pickup.connect("gun_picked", self, "_on_gun_picked")
	gun_sprite.modulate.a = 0
	dialogue.connect("closed", self, "_on_closed")

func _on_gun_picked():
	chest.frame += 1
	$Barrier.queue_free()
	pickup.call_deferred("free")

func _process(_delta):
	if hero.position.y > OS.window_size.y and hero.gun != null:
		emit_signal("proceed")
	elif hero.position.y > OS.window_size.y and hero.gun == null:
		 emit_signal("back")
	
	if talk.visible and Input.is_action_pressed("hero_pickup"):
		dialogue.open()
		talk.hide()
		been_there = true

func _on_Area2D_body_entered(body):
	if(body.has_method("move_and_slide") and !been_there):
		talk.show()

func _on_Area2D_body_exited(body):
	if(body.has_method("move_and_slide") and !been_there):
		talk.hide()

func _on_closed():
	talk.hide()
