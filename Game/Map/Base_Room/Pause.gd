extends Control

onready var paused : bool = false 

func _input(event):
	if Input.is_action_just_pressed("pause") and !paused:
		get_tree().paused = true
		$Tween.interpolate_property($ColorRect, "modulate:a", 0, 0.5, 0.5)
		$Label.show()
		$Tween.start()

	if Input.is_action_just_pressed("pause") and paused:
		get_tree().paused = false
		$Tween.interpolate_property($ColorRect, "modulate:a", 0.5, 0, 0.5)
		$Label.hide()
		$Tween.start()


func _on_Tween_tween_all_completed():
	paused = !paused
