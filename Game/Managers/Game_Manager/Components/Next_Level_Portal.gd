extends Area2D

var on_area : bool = false
var available : bool = false setget change_available

signal portal_activated

func change_available(aval : bool):
	if not aval:
		self.hide()
		$Portal.emitting = false
		available = false

	else:
		self.show()
		$Portal.restart()
		available = true

func _on_Next_Level_Portal_body_entered(_body):
	on_area = true
	$Label.show()

func _on_Next_Level_Portal_body_exited(_body):
	on_area = false
	$Label.hide()

func _input(event):
	if available and on_area and event.is_action_pressed("hero_pickup"):
		emit_signal("portal_activated")
