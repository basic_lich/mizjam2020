extends Button

export(PackedScene) var scene_to_load

onready var label = $Label
onready var hover = $Char_Hover 
onready var anim = $Char_Hover/AnimationPlayer

func _ready():
	anim.play("Button_Hover")

func _on_Label_mouse_entered():
	label.set("custom_colors/font_color", Color(0, 1, 0))
	hover.visible = true

func _on_Label_mouse_exited():
	label.set("custom_colors/font_color", Color(1, 1, 1))
	hover.visible = false

func _on_Label_focus_entered():
	label.set("custom_colors/font_color", Color(0, 1, 0))
	hover.visible = true

func _on_Label_focus_exited():
	label.set("custom_colors/font_color", Color(1, 1, 1))
	hover.visible = false

