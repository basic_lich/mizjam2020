extends Sprite

const SPD : float = 20.0
const damage : int = 1

var direction : Vector2

func _ready():
	direction = direction.rotated(rand_range(-0.5, 0.5))

func _physics_process(_delta):
	self.position += direction*SPD

func _on_Visible_screen_exited():
	self.call_deferred("free")

func _on_Hitbox_body_entered(body):
	if body is Enemy:
		body.damage(self.damage)

	self.call_deferred("free")
