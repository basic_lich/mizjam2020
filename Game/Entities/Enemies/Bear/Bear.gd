extends Enemy

var speed : int = 600

var direction : Vector2
var movement : Vector2
var stop : bool

var knockback_multiplier : int = 1
var on_knockback : bool = false
var knockback_timer : float = 0.0
const KNOCKBACK_DUR : float = 0.2

func _ready():
	raging()

func _physics_process(delta):
	if stop:
		movement = Vector2(0,0)
		var vetor = (hero.global_position - self.global_position).normalized()
		self.rotation = vetor.angle()
		if vetor.x < 0:
			self.rotation -= PI

		direction = vetor

	elif !stop:
		movement = direction * speed

	if on_knockback:
		movement *= -0.3 * knockback_multiplier
		knockback_timer += delta

		if knockback_timer >= KNOCKBACK_DUR:
			knockback_timer = 0
			on_knockback = false

	self.move_and_slide(movement)

func damage(value, knockback = false):
	.damage(value)

	if knockback:
		on_knockback = true
		knockback_multiplier = 2
		$Timer.start()

func raging():
	stop = true
	$WaitTime.start()
	$AnimationPlayer.play("rage")

func _on_Hitbox_body_entered(body):
	body.damage(self.global_position)
	on_knockback = true
	raging()

func _on_Timer_timeout():
	knockback_multiplier = 1

func _on_RunTime_timeout():
	raging()

func _on_WaitTime_timeout():
	$AnimationPlayer.play("alert")
	$AlertTime.start()

func _on_AlertTime_timeout():
	stop = false
	$RunTime.start()
