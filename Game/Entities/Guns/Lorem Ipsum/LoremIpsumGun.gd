extends Gun_Base

signal created_projectile(letter)

export (PackedScene) var letters_scene
export (int) var speed

onready var cooldown = $Cooldown
onready var sprite = $Sprite

func _physics_process(delta):
	rotate_gun()
	if (get_global_mouse_position().x - self.global_position.x) < 0:
		sprite.flip_v = true
	else:
		sprite.flip_v = false

func shoot():
	if !cooldown.is_stopped():
		return
	cooldown.start()
	$CPUParticles2D.emitting = true
	for i in range(0, 10):
		var letter = letters_scene.instance()
		if (get_global_mouse_position().x - self.global_position.x) < 0:
			letter.setup(sprite.global_position, rotation_degrees, true, i)
		else:
			letter.setup(sprite.global_position, rotation_degrees, false, i)
		emit_signal("created_projectile", letter)
		$Timer.start()
		yield(get_node("Timer"), "timeout")
	$CPUParticles2D.emitting = false
