extends Enemy

var speed : int = 300
export(Vector2) var seek_point : Vector2 = Vector2.ZERO

var knockback_multiplier : int = 1
var on_knockback : bool = false
var knockback_timer : float = 0.0
const KNOCKBACK_DUR : float = 0.2

func _physics_process(delta):
	var target = hero.global_position
	self.look_at(target)

	var dir = (target - self.global_position)

	var seek_dir = Vector2.ZERO
	if dir.length() > 150: seek_dir = seek_point.rotated(self.rotation)

	var direction = (target + seek_dir - self.global_position).normalized()
	var movement = direction * speed

	if on_knockback:
		movement *= -1 * knockback_multiplier
		knockback_timer += delta

		if knockback_timer >= KNOCKBACK_DUR:
			knockback_timer = 0
			on_knockback = false

	# warning-ignore:return_value_discarded
	self.move_and_slide(movement)

func damage(value, knockback = false):
	.damage(value)

	if knockback:
		on_knockback = true
		knockback_multiplier = 2
		$Timer.start()

func _on_Hitbox_body_entered(body):
	body.damage(self.global_position)
	on_knockback = true

func _on_Timer_timeout():
	knockback_multiplier = 1
