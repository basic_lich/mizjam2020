extends Gun_Base

var damage : int = 10

var looking_left : bool = false
var attacking : bool = false

func _physics_process(_delta):
	if not attacking:
		rotate_gun()

		if (get_global_mouse_position().x - self.global_position.x) < 0:
			$Sprite.rotation_degrees = 135
			looking_left = true
		else:
			$Sprite.rotation_degrees = -45
			looking_left = false

func shoot():
	if not attacking:
		attacking = true
		var sword_rotation : int = 180
		if looking_left: sword_rotation *= -1
		$Tween.interpolate_property($Sprite, "rotation_degrees",
									$Sprite.rotation_degrees, $Sprite.rotation_degrees+sword_rotation,
									0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$Tween.start()
		$Hitbox/Area.call_deferred("set", "disabled", false)
		$Cooldown.start()

func _on_Cooldown_timeout():
	attacking = false
	$Hitbox/Area.call_deferred("set", "disabled", true)

func _on_Hitbox_body_entered(body):
	if(body.has_method("is_enemy")):
		body.damage(damage)
	#deal damage
