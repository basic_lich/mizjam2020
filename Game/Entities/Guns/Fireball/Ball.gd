extends Node2D

var speed : int = 600
var direction := Vector2(1, 0)
var damage : float = 5

func _process(delta):
	position += speed * delta * direction

func setup(pos:Vector2, angle:float, mod, n) -> void:
	$Sprite.modulate = mod
	$CPUParticles2D.modulate = mod
	$CPUParticles2D2.modulate = mod
	position = pos
	rotation_degrees = angle
	direction = direction.rotated(deg2rad(angle))
	match n:
		1:
			damage *= 1.5
		2:
			speed *= 1.2

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_Hitbox_body_entered(body):
	speed = 0
	$Sprite.modulate.a = 0
	$CPUParticles2D.emitting = true
	$Tween.interpolate_property($Explosion, "scale", Vector2.ZERO, Vector2.ONE, 0.75)
	$Tween.start()

func _on_Explosion_body_entered(body):
	if(body.has_method("damage")):
		body.damage(damage)
	$CPUParticles2D2.emitting = false

func _on_Tween_tween_all_completed():
	self.queue_free()
