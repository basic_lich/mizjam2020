extends Gun_Base

signal created_projectile(dice)

export(PackedScene) var dice_scene


func _physics_process(delta):
	rotate_gun()
	while $Timer.is_stopped():
		$Sprite.frame += 1
		$Timer.start()
		if $Sprite.frame == 768:
			$Sprite.frame = 762


func shoot():
	if !$Cooldown.is_stopped():
		return
	$Cooldown.start()
	var dice = dice_scene.instance()
	dice.setup($Sprite.global_position, rotation_degrees, $Sprite.frame)
	emit_signal("created_projectile", dice)
