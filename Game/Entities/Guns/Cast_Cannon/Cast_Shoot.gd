extends Node2D

export(float) var speed = 1000

onready var particles = $Particles2D
onready var sfx = $Sfx

var direction : Vector2 = Vector2.RIGHT
var damage : float
var particle_radius : float

func _process(delta):
	position += direction * speed * delta
	sfx.play(0.0)

func setup(pos: Vector2, angle: float, power: float, radius : float) -> void:
	position = pos
	rotation_degrees = angle
	damage = power
	particle_radius = radius
	direction = direction.rotated(deg2rad(angle))

func _ready():
	particles.process_material.emission_sphere_radius = particle_radius

func _on_HitBox_body_entered(body):
	if(body.has_method("damage")):
		body.damage(damage)
	print("collided")
	queue_free()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
