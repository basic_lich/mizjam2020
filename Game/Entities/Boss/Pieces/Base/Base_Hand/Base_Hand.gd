extends Enemy

export(float) var hero_distance : float = 100.0

var speed : Vector2 = Vector2.ZERO
export(float) var accel : float = 1500.0
export(float) var MAX_SPEED : float = 800.0

var chasing : bool = true

class_name Base_Hand

func _physics_process(delta):
	var dist = (hero.global_position) - self.global_position

	if dist.length() > hero_distance:
		speed += dist.normalized()*accel*delta
		speed = speed.clamped(MAX_SPEED)

	if not chasing:
		speed *= 0.9

	self.move_and_slide(speed)
