extends Node2D

var player : Hero

enum {INTRO, PHASE1, TRAN, PHASE2, GLITCH, FINAL}
var state : int = INTRO

var boss

func _ready():
	get_tree().paused = true

	player = Global.hero
	$Game.add_child(player)
	$Game/Life.hero = player
	$Game/Boss.hero = player
	for piece in $Game/Boss.get_node("Pieces").get_children():
		piece.hero = player

	player.global_position = Vector2(1280, 1200)

	# warning-ignore:return_value_discarded
	player.connect("created_projectile", self, "create")
	# warning-ignore:return_value_discarded
	player.connect("died", self, "player_dead")
	# warning-ignore:return_value_discarded
	$Game/Boss.connect("created_projectile", self, "create")

	$Panel.modulate.a = 1
	$Intro.modulate.a = 1

	$Cutscene_Timer.start()
	yield($Cutscene_Timer, "timeout")

	$Tween.interpolate_property($Panel, "modulate:a", 1, 0, 3)
	$Tween.interpolate_property($Intro, "modulate:a", 1, 0, 3)
	$Tween.start()

func player_dead():
	if state == PHASE2:
		for thing in $Things.get_children():
			thing.call_deferred("free")

		get_tree().paused = true
		player.call_deferred("free")
		player = load("res://Entities/Hero/Hero.tscn").instance()
		# warning-ignore:return_value_discarded
		player.get_gun(load("res://Entities/Guns/Glitch/Glitch_Gun.tscn").instance())
		$Game.add_child(player)

		# warning-ignore:return_value_discarded
		player.connect("created_projectile", self, "create")
		# warning-ignore:return_value_discarded
		player.connect("died", self, "player_dead")

		$Game/Life.hero = player

		player.global_position = Vector2(1280, 1200)
		boss.position = Vector2(1280, 650)
		boss.rotation = 0
		boss.speed = Vector2.ZERO
		boss.hero = player

		state = GLITCH

		$Panel.modulate.a = 1
		$Glitch.modulate.a = 1

		$Cutscene_Timer.start()
		yield($Cutscene_Timer, "timeout")

		$Tween.interpolate_property($Panel, "modulate:a", 1, 0, 3)
		$Tween.interpolate_property($Glitch, "modulate:a", 1, 0, 3)
		$Tween.start()
	
	else:
		player.die()

func create(thing):
	$Things.add_child(thing)

func _on_Boss_died():
	for thing in $Things.get_children():
		thing.call_deferred("free")

	var phase2 = load("res://Entities/Boss/Phase2/Boss_2.tscn").instance()
	phase2.position = Vector2(1280, 650)
	player.global_position = Vector2(1280, 1200)
	phase2.hero = player
	phase2.connect("created_projectile", self, "create")
	phase2.connect("died",self,"_on_Boss_2_died")

	boss = phase2

	$Game/Boss.call_deferred("free")
	$Game.call_deferred("add_child", phase2)

	get_tree().paused = true

	$Panel.modulate.a = 1
	$Phase2.modulate.a = 1

	$Cutscene_Timer.start()
	yield($Cutscene_Timer, "timeout")

	$Tween.interpolate_property($Panel, "modulate:a", 1, 0, 3)
	$Tween.interpolate_property($Phase2, "modulate:a", 1, 0, 3)
	$Tween.start()

func _on_Boss_2_died():
	get_tree().paused = true
	$Panel.modulate.a = 1
	$Victory.modulate.a = 1

func _on_Tween_all_completed():
	match(state):
		INTRO:
			get_tree().paused = false
			state = PHASE1

		PHASE1:
			get_tree().paused = false
			state = PHASE2

		GLITCH:
			get_tree().paused = false
			state = PHASE2
