extends Gun_Base

const MAX_POWER = 5
const FLOAT_EPSILON = 0.00001

signal created_projectile(cast)

export(PackedScene) var cast_scene

onready var sprite = $Sprite
onready var ammo = $Ammo
onready var dmg_cooldown = $Dmg_cooldown
onready var tween = $Tween
onready var particles = $Particles2D
onready var ray_sfx = $Ray_sfx

enum {GROWING, MAX}
var state : int = GROWING

export(float) var power

func _physics_process(delta):
	rotate_gun()

	if get_global_mouse_position().x - self.global_position.x < 0:
		sprite.flip_v = true
		ammo.position.y = 13
		particles.position.y = 13

	else:
		sprite.flip_v = false
		ammo.position.y = 0
		particles.position.y = 0

func _input(event):
	if event.is_action_pressed("hero_shoot"):
		shoot()
		ammo_grow()

	elif event.is_action_released("hero_shoot"):
		hero_damage()
		particles.emitting = false

func shoot():
	dmg_cooldown.start()
	ammo.visible = true
	ammo.position = Vector2(100, 0)
	ammo.rotation_degrees = 270
	particles.position = Vector2(96, 0)
	particles.process_material.emission_sphere_radius = 0.01
	ray_sfx.volume_db = -40

func ammo_grow():
	particles.restart()
	state = GROWING
	tween.interpolate_property(ammo, "scale", Vector2(0, 0), Vector2(MAX_POWER, MAX_POWER), 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.interpolate_property(ammo, "position:x", ammo.position.x, 128, 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.interpolate_property(ammo, "rotation_degrees", ammo.rotation_degrees, 1080, 1, Tween.TRANS_BOUNCE, Tween.EASE_OUT_IN)
	tween.interpolate_property(particles.process_material, "emission_sphere_radius", 0, 10, 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.interpolate_property(particles, "position:x", particles.position.x, 124, 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.interpolate_property(ray_sfx, "volume_db", -60, 0, 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	ray_sfx.play(0.0)

func hero_damage():
	var percentage : float = dmg_cooldown.wait_time - dmg_cooldown.time_left
	var cast = cast_scene.instance()
	dmg_cooldown.stop()
	power = percentage * MAX_POWER
	cast.setup(ammo.global_position, rotation_degrees, power, particles.process_material.emission_sphere_radius)
	cast.get_child(0).scale = Vector2(power, power)
	ammo.visible = false
	ray_sfx.stop()
	emit_signal("created_projectile", cast)

func _on_Dmg_cooldown_timeout():
	dmg_cooldown.stop()


func _on_Tween_tween_all_completed():
	match(state):
		GROWING:
			tween.interpolate_property(ammo, "rotation_degrees", ammo.rotation_degrees, 0, 0.1, Tween.TRANS_BOUNCE, Tween.EASE_OUT_IN)
			tween.interpolate_property(ammo, "position:x", ammo.position.x, 124, 0.1, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
			tween.interpolate_property(ammo, "position:y", ammo.position.y, 4, 0.1, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
			tween.interpolate_property(particles, "position:x", particles.position.x, 120, 0.1, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
			tween.interpolate_property(particles, "position:y", particles.position.y, 4, 0.1, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
			tween.start()
			state = MAX
		MAX:
			tween.interpolate_property(ammo, "rotation_degrees", ammo.rotation_degrees, 1080, 0.1, Tween.TRANS_BOUNCE, Tween.EASE_OUT_IN)
			tween.interpolate_property(ammo, "position:x", ammo.position.x, 128, 0.1, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
			tween.interpolate_property(ammo, "position:y", ammo.position.y, -4, 0.1, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
			tween.interpolate_property(particles, "position:x", particles.position.x, 128, 0.1, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
			tween.interpolate_property(particles, "position:y", particles.position.y, -4, 0.1, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
			tween.start()
			state = GROWING
