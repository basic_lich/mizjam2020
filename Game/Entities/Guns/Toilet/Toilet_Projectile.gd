extends Node2D

var damage = 10

const FALL_TIME : float = 1.0

enum {TELE, FALLING, CRASHED}
var state : int

var target : Vector2

func setup(pos : Vector2, set_target : Vector2):
	self.global_position = pos
	self.target = set_target

func _ready():
	state = TELE
	$Tween.interpolate_property($Toilet, "position:y", 0, -2000, 0.5, Tween.TRANS_EXPO, Tween.EASE_IN)
	$Tween.start()

func _on_Tween_tween_all_completed():
	match(state):

		TELE:
			self.global_position = target
			$Shadow.modulate.a = 0
			$Shadow.show()
			$Tween.interpolate_property($Toilet, "position:y", -2000, 0, FALL_TIME, Tween.TRANS_EXPO, Tween.EASE_IN)
			$Tween.interpolate_property($Toilet, "scale", Vector2(0.01, 0.01), Vector2(6, 6), FALL_TIME)
			$Tween.interpolate_property($Shadow, "scale", Vector2(20, 20), Vector2(5, 5), FALL_TIME)
			$Tween.interpolate_property($Shadow, "modulate:a", 0, 0.6, FALL_TIME)
			$Tween.start()
			state = FALLING

		FALLING:
			$Crash_Particles.restart()
			$Shadow.hide()
			$Tween.interpolate_property($Toilet, "scale", $Toilet.scale, Vector2(40, 40), 0.1)
			$Tween.interpolate_property($Toilet, "modulate:a", 1, 0, 0.05)
			$Tween.start()
			$Hitbox/Area.call_deferred("set", "disabled", false)
			state = CRASHED

		CRASHED:
			self.call_deferred("free")

func _on_Hitbox_body_entered(body):
	body.damage(damage)
