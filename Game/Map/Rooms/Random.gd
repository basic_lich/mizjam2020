extends Node2D

export(Array, PackedScene) var Available_Guns : Array = []

func _ready():
	Available_Guns.shuffle()
	$"../Gun_Pickup".gun_scene = Available_Guns[0]
	$"../Gun_Pickup2".gun_scene = Available_Guns[1]
	$"../Gun_Pickup3".gun_scene = Available_Guns[2]
