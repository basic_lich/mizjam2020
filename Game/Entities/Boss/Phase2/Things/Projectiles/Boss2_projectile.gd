extends Area2D

var hero : Hero
var speed : Vector2 = Vector2.ZERO

var dead : bool = false

func _physics_process(delta):
	if not dead:
		speed += (hero.global_position - self.global_position).normalized()*50*delta
		speed = speed.clamped(200)
		self.position += speed
		self.rotation = speed.angle()

func _on_Boss2_projectile_body_entered(body):
	body.damage(self.global_position)
	death()

func _on_Timer_timeout():
	death()

func death():
	dead = true
	$Sprite.hide()
	$area.call_deferred("set", "disabled", true)
	$Particles2D.restart()
	$Death_Timer.start()

func _on_Death_Timer_timeout():
	self.call_deferred("free")
