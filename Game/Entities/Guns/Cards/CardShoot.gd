extends Node2D

onready var tween = get_node("Tween")

var rotation_speed : int = 2000
var spin_direction : int = 1
var speed : int = 600
var direction := Vector2(1, 0)
var damage
onready var stop = false

func _process(delta):
	if !stop:
		position += speed * delta * direction
		#self.rotation_degrees += rotation_speed * spin_direction * delta

func setup(pos:Vector2, angle:float, frame, mod) -> void:
	$Sprite.frame = frame
	$Sprite.modulate = mod
	$CPUParticles2D.modulate = mod
	calc_damage($Sprite.frame_coords.x)
	position = pos
	rotation_degrees = angle
	direction = direction.rotated(deg2rad(angle))

func calc_damage(card):
	if card == 20:
		damage = 5
	elif card > 30:
		damage = 4
	else:
		damage = 3

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_Area2D_body_entered(body):
	if(body.has_method("damage")):
		body.damage(damage)
	stop = true
	tween.interpolate_property(self, "modulate:a", 1, 0, 1, Tween.TRANS_QUART, Tween.EASE_IN_OUT)
	tween.start()
	$Area2D.queue_free()

func _on_Tween_tween_all_completed():
	queue_free()
