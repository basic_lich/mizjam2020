extends Enemy

signal created_projectile(projectile)

var speed : Vector2 = Vector2.ZERO

func _ready():
	self.health = 0
	for part in $Pieces.get_children():
		self.health += 1
		part.connect("died", self, "part_dead")
		if part.is_in_group("boss"):
			part.connect("created_projectile", self, "part_projectile")

func _physics_process(delta):
	var dist = hero.global_position - $Helmet.global_position
	$Helmet.rotation = ($Helmet.rotation)*0.9 + (dist.angle() - 1.57)*0.1

	if dist.length() < 500:
		speed += dist.normalized()*400*delta
		speed = speed.clamped(500)
		self.move_and_slide(-speed)

	else:
		speed *= 0.9

func part_projectile(project):
	emit_signal("created_projectile", project)

func damage(_damage):
	pass

func part_dead():
	self.health -= 1
	if health <= 0:
		emit_signal("died")
