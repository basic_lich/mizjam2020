extends Gun_Base

export(PackedScene) var projectile

signal created_projectile(toilet)

var on_cooldown : bool = false

func _physics_process(delta):
	rotate_gun()
	$Sprite.rotation = -self.rotation

func shoot():
	if not on_cooldown:
		on_cooldown = true
		var toilet = projectile.instance()
		toilet.setup($Sprite.global_position, get_global_mouse_position())
		emit_signal("created_projectile", toilet)
		$Sprite.modulate.a = 0
		$Cooldown.start()

func _on_Cooldown_timeout():
	$Tween.interpolate_property($Sprite, "modulate:a", 0, 1, 0.2)
	$Tween.interpolate_property($Sprite, "scale", Vector2(0.01, 0.01), Vector2.ONE, 0.5, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
	$Tween.start()

func _on_Tween_tween_all_completed():
	on_cooldown = false
