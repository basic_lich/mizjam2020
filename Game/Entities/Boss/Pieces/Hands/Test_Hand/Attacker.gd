extends Node2D

var atk_ready : bool = true

onready var hand : Enemy = get_parent()
onready var anim : AnimationPlayer = get_parent().get_node("Anim")

func _physics_process(delta):
	var dist : Vector2 = (hand.hero.global_position - hand.global_position)
	if atk_ready and dist.length() <= hand.hero_distance:
			hand.chasing = false
			anim.play("Grab")

	if hand.scale.y < 0 : hand.rotation = dist.angle()-1.57+PI
	else: hand.rotation = dist.angle()-1.57

func _on_Anim_animation_finished(anim_name):
	if anim_name == "Grab":
		anim.play("Idle")
		hand.speed = Vector2.ZERO
		hand.chasing = true
		atk_ready = true

func _on_Hitbox_body_entered(body):
	body.damage(hand.global_position)
