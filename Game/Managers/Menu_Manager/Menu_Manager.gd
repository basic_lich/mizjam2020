extends Control

export(PackedScene) var start_scene

onready var title = $Title_Screen
onready var credits = $Credits


func _ready():
	credits.visible = false
	
	credits.connect("go_back", self, "_on_back")

func _on_back():
	credits.visible = false
	title.visible = true

func _on_Title_Screen_go_credits():
	title.visible = false
	credits.visible = true
