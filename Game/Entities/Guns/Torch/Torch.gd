extends Gun_Base

var cooldown : bool = false
var shooting : bool = false

const MAX_HEAT : int = 100
const HEAT_SEC : float = 45.0
var heat : float = 0.0

export(PackedScene) var heat_node

func _physics_process(delta):
	rotate_gun()

	if not shooting:
		if (get_global_mouse_position().x - self.global_position.x) < 0:
			$Sprite.rotation_degrees = 135

		else:
			$Sprite.rotation_degrees = -45

		heat -= HEAT_SEC*delta
		heat = max(heat, 0)
		var hpc : float = heat/MAX_HEAT
		$Sprite.scale = Vector2(1,1) + hpc*Vector2(0.2,0.2)
		$Sprite.modulate = Color(0.941176*(1-hpc), 0.854902*(1-hpc), 0.207843*(1-hpc)) + Color(1*hpc, 0, 0)

		if cooldown and heat == 0:
			cooldown = false

	else:
		heat += HEAT_SEC*delta
		heat = min(heat, MAX_HEAT)
		var hpc : float = heat/MAX_HEAT
		$Sprite.scale = Vector2(1,1) + hpc*Vector2(0.2,0.2)
		$Sprite.modulate = Color(0.941176*(1-hpc), 0.854902*(1-hpc), 0.207843*(1-hpc)) + Color(1*hpc, 0, 0)

		if heat == MAX_HEAT:
			cooldown = true
			shooting = false
			$Sprite/Attack_Particles.emitting = false
			$Proc_Burn.stop()

func _input(event):
	if not cooldown:
		if event.is_action_pressed("hero_shoot"):
			$Sprite/Attack_Particles.emitting = true
			$Sprite.rotation_degrees = 45
			shooting = true
			$Proc_Burn.start()

		elif event.is_action_released("hero_shoot"):
			$Sprite/Attack_Particles.emitting = false
			shooting = false
			$Proc_Burn.stop()

func _on_Proc_Burn_timeout():
	for body in $Sprite/Hitbox.get_overlapping_bodies():

		if body.has_node("Heat"):
			body.get_node("Heat").increase_heat()

		else:
			var new_heat = heat_node.instance()
			body.add_child(new_heat)
