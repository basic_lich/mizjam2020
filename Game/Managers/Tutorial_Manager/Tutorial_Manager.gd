extends Node2D

export(PackedScene) var actual_game_scene
export(PackedScene) var hero_scene

var rooms : Array = []
var cur_room
var player : Hero
var changing : bool = false
var monsterfying: bool = false
var posy : int

enum{ROOM0, ROOM1, ROOM2}
var room_state : int = ROOM0

onready var game = $Tutorial_Game

func _ready():
	get_tree().paused = false

	$Tween.interpolate_property(	$Panel, "rect_position",
								Vector2.ZERO, OS.window_size/2,
								1.5, Tween.TRANS_ELASTIC)
	$Tween.interpolate_property(	$Panel, "rect_size",
								OS.window_size, Vector2.ZERO,
								1.5, Tween.TRANS_ELASTIC)
	$Tween.start()

	var path : String = ""
	var dir : Directory = Directory.new()

	path = "res://Map/Tutorial/Rooms_Scenes/"
	if dir.open(path) == OK:
		dir.list_dir_begin(true, true)
		var file_name : String = dir.get_next()
		while file_name != "":
			rooms.append(path + file_name)
			file_name = dir.get_next()

	else:
		print("An error occurred when trying to access the battle path.")

	dir.list_dir_end()

	player = hero_scene.instance()
	player.position = Vector2(128, 352)
	game.add_child(player)

	cur_room = load(rooms[0]).instance()
	cur_room.hero = player
	cur_room.connect("proceed", self, "_on_proceed")
	cur_room.connect("back", self, "_on_back")
	game.add_child(cur_room)

func changing_level(monster, change):
	changing = change
	monsterfying = monster
	$Tween.interpolate_property(	$Panel, "rect_position",
								OS.window_size/2, Vector2.ZERO,
								0.5, Tween.TRANS_EXPO)
	$Tween.interpolate_property(	$Panel, "rect_size",
								Vector2.ZERO, OS.window_size,
								0.5, Tween.TRANS_EXPO)
	$Tween.start()

func _on_proceed():
	get_tree().paused = true

	if room_state < 2:
		room_state += 1

	changing_level(false, true)

func _on_back():
	get_tree().paused = true

	if room_state > 0:
		room_state -= 1

	changing_level(false, true)

func _on_monsterfy():
	get_tree().paused = true

	changing_level(true, false)

func _on_Tween_tween_all_completed():
	if changing:
		changing = false

		match(room_state):
			ROOM0:
				player.position.y = 640
			ROOM1:
				player.position.y = 128
			ROOM2:
				player.position.y = 640

		game.remove_child(cur_room)
		cur_room.call_deferred("free")

		cur_room = load(rooms[room_state]).instance()
		cur_room.hero = player
		cur_room.connect("proceed", self, "_on_proceed")
		cur_room.connect("back", self, "_on_back")
		cur_room.connect("monsterfy", self, "_on_monsterfy")
		game.add_child(cur_room)

		$Tween.interpolate_property(	$Panel, "rect_position",
								Vector2.ZERO, OS.window_size/2,
								1.5, Tween.TRANS_ELASTIC)
		$Tween.interpolate_property(	$Panel, "rect_size",
								OS.window_size, Vector2.ZERO,
								1.5, Tween.TRANS_ELASTIC)
		$Tween.start()

	elif monsterfying:
		get_tree().change_scene_to(actual_game_scene)

	else: get_tree().paused = false
