extends Area2D

export(PackedScene) var gun_scene

var gun : Gun_Base

var hero : Hero
var on_area : bool = false

signal gun_picked

func _ready():
	gun = gun_scene.instance()
	$Sprite.frame = gun.get_node("Sprite").frame
	$Sprite.modulate = gun.get_node("Sprite").modulate

func _on_Gun_Pickup_body_entered(body):
	hero = body
	on_area = true
	$Pickup_Button.show()

func _on_Gun_Pickup_body_exited(_body):
	hero = null
	on_area = false
	$Pickup_Button.hide()

func _input(event):
	if on_area and event.is_action_pressed("hero_pickup") and gun != null:
		gun = hero.get_gun(gun)
		emit_signal("gun_picked")

		if gun == null:
			self.call_deferred("free")

		else:
			$Sprite.frame = gun.get_node("Sprite").frame
			$Sprite.modulate = gun.get_node("Sprite").modulate
